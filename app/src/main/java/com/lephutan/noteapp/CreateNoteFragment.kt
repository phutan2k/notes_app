package com.lephutan.noteapp

import android.Manifest
import android.annotation.SuppressLint
import android.app.Activity
import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import android.graphics.BitmapFactory
import android.graphics.Color
import android.net.Uri
import android.os.Bundle
import android.provider.MediaStore
import android.util.Patterns
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.localbroadcastmanager.content.LocalBroadcastManager
import com.lephutan.noteapp.database.NotesDatabase
import com.lephutan.noteapp.databinding.FragmentCreateNoteBinding
import com.lephutan.noteapp.entities.Notes
import kotlinx.coroutines.launch
import pub.devrel.easypermissions.AppSettingsDialog
import pub.devrel.easypermissions.EasyPermissions
import java.text.SimpleDateFormat
import java.util.*

class CreateNoteFragment : BaseFragment(), EasyPermissions.PermissionCallbacks,
    EasyPermissions.RationaleCallbacks {
    private var _binding: FragmentCreateNoteBinding? = null
    private val binding get() = _binding!!

    var currentDate: String? = null

    // Select default Color
    var selectedColor = "#171C26"

    private var READ_STORAGE_PERM = 123
    private var REQUEST_CODE_IMAGE = 456
    private var selectedImagePath = ""
    private var webLink = ""
    private var noteId = -1

    companion object {
        fun newInstance(): CreateNoteFragment {
            return CreateNoteFragment()
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

//        noteId = requireArguments().getInt("noteId", -1)
//        Log.i("TanTan", "onCreate: $noteId")

        val argument = arguments

        if (argument == null) {
            noteId = -1
        } else {
            noteId = requireArguments().getInt("noteId")
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        _binding = FragmentCreateNoteBinding.inflate(inflater, container, false)

        return binding.root
    }

    @SuppressLint("SimpleDateFormat")
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        LocalBroadcastManager.getInstance(requireContext()).registerReceiver(
            broadcastReceiver, IntentFilter("bottom_sheet_action")
        )

        val simpleDateFormat = SimpleDateFormat("dd/M/yyyy hh:mm:ss")
        currentDate = simpleDateFormat.format(Date())

        binding.tvDateTime.text = currentDate
        binding.vColorView.setBackgroundColor(Color.parseColor(selectedColor))

        clickViews()
        showSpecificNoteById()
    }

    private fun showSpecificNoteById() {
        if (noteId != -1) {
            launch {
                context?.let {
                    val noteItem = NotesDatabase.getDatabase(it).noteDao().getSpecificNote(noteId)

                    binding.apply {
                        vColorView.setBackgroundColor(Color.parseColor(noteItem.color))
                        edtNoteTitle.setText(noteItem.title)
                        edtNoteSubTitle.setText(noteItem.subTitle)
                        edtNoteDesc.setText(noteItem.noteText)
                    }

                    if (!noteItem.imgPath.isNullOrEmpty()) {
                        selectedImagePath = noteItem.imgPath!!

                        binding.apply {
                            ivNote.setImageBitmap(BitmapFactory.decodeFile(noteItem.imgPath))
                            rlLayoutImage.visibility = View.VISIBLE
                            ivNote.visibility = View.VISIBLE
                            ivDelete.visibility = View.VISIBLE
                        }
                    } else {
                        binding.apply {
                            rlLayoutImage.visibility = View.GONE
                            ivNote.visibility = View.GONE
                            ivDelete.visibility = View.GONE
                        }
                    }

                    if (!noteItem.webLink.isNullOrEmpty()) {
                        webLink = noteItem.webLink!!

                        binding.apply {
                            tvWebLink.text = noteItem.webLink
                            lnLayoutWebUrl.visibility = View.VISIBLE
                            edtWebLink.setText(noteItem.webLink)
                            ivUrlDelete.visibility = View.VISIBLE
                        }
                    } else {
                        binding.ivUrlDelete.visibility = View.GONE
                        binding.lnLayoutWebUrl.visibility = View.GONE
                    }
                }
            }
        }
    }

    private fun clickViews() {
        binding.ivDone.setOnClickListener {
            if (noteId != -1) updateNote() else saveNote()
        }

        binding.ivBack.setOnClickListener {
            requireActivity().supportFragmentManager.popBackStack()
        }

        binding.ivMore.setOnClickListener {
            val noteBottomSheetFragment = NoteBottomSheetFragment.newInstance(noteId)

            noteBottomSheetFragment.show(
                requireActivity().supportFragmentManager,
                "Note Bottom Sheet Fragment"
            )
        }

        binding.btnOk.setOnClickListener {
            if (binding.edtWebLink.text.toString().trim().isNotEmpty()) {
                checkWebUrl()
            } else {
                notify("Url is Required")
            }
        }

        binding.btnCancel.setOnClickListener {
            if (noteId != -1) {
                binding.tvWebLink.visibility = View.VISIBLE
                binding.lnLayoutWebUrl.visibility = View.GONE
            } else {
                binding.lnLayoutWebUrl.visibility = View.GONE
            }
        }

        binding.tvWebLink.setOnClickListener {
            val intent = Intent(Intent.ACTION_VIEW, Uri.parse(binding.edtWebLink.text.toString()))
            startActivity(intent)
        }

        binding.ivDelete.setOnClickListener {
            selectedImagePath = ""
            binding.rlLayoutImage.visibility = View.GONE
        }

        binding.ivUrlDelete.setOnClickListener {
            webLink = ""
            binding.apply {
                tvWebLink.visibility = View.GONE
                ivUrlDelete.visibility = View.GONE
                lnLayoutWebUrl.visibility = View.GONE
            }
        }
    }

    private fun updateNote() {
        launch {

            context?.let {
                val noteResult = NotesDatabase.getDatabase(it).noteDao().getSpecificNote(noteId)

                noteResult.title = binding.edtNoteTitle.text.toString()
                noteResult.subTitle = binding.edtNoteSubTitle.text.toString()
                noteResult.noteText = binding.edtNoteDesc.text.toString()
                noteResult.dateTime = currentDate
                noteResult.color = selectedColor
                noteResult.imgPath = selectedImagePath
                noteResult.webLink = webLink

                NotesDatabase.getDatabase(it).noteDao().updateNote(noteResult)

                binding.apply {
                    edtNoteTitle.setText("")
                    edtNoteSubTitle.setText("")
                    edtNoteDesc.setText("")
                    rlLayoutImage.visibility = View.GONE
                    ivNote.visibility = View.GONE
                    tvWebLink.visibility = View.GONE
                }
                requireActivity().supportFragmentManager.popBackStack()
            }
        }
    }

    private fun saveNote() {
        if (binding.edtNoteTitle.text.isNullOrEmpty()) {
            notify("Note Title is Required")
        } else if (binding.edtNoteSubTitle.text.isNullOrEmpty()) {
            notify("Note Sub Title is Required")
        } else if (binding.edtNoteDesc.text.isNullOrEmpty()) {
            notify("Note Description is Required")
        } else {
            launch {
                val notes = Notes()

                notes.title = binding.edtNoteTitle.text.toString()
                notes.subTitle = binding.edtNoteSubTitle.text.toString()
                notes.noteText = binding.edtNoteDesc.text.toString()
                notes.dateTime = currentDate
                notes.color = selectedColor
                notes.imgPath = selectedImagePath
                notes.webLink = webLink

                context?.let {
                    NotesDatabase.getDatabase(it).noteDao().insertNotes(notes)
                    binding.apply {
                        edtNoteTitle.setText("")
                        edtNoteSubTitle.setText("")
                        edtNoteDesc.setText("")
                        rlLayoutImage.visibility = View.GONE
                        ivNote.visibility = View.GONE
                        tvWebLink.visibility = View.GONE
                        requireActivity().supportFragmentManager.popBackStack()
                    }
                }
            }
        }
    }

    private fun checkWebUrl() {
        binding.apply {
            if (Patterns.WEB_URL.matcher(edtWebLink.text.toString()).matches()) {
                lnLayoutWebUrl.visibility = View.GONE
                edtWebLink.isEnabled = false
                webLink = edtWebLink.text.toString()
                tvWebLink.visibility = View.VISIBLE
                tvWebLink.text = edtWebLink.text.toString()
            } else {
                notify("Url is not valid")
            }
        }
    }

    private val broadcastReceiver: BroadcastReceiver = object : BroadcastReceiver() {
        override fun onReceive(p0: Context?, p1: Intent?) {
            val actionColor = p1!!.getStringExtra("action")

            when (actionColor!!) {
                "Blue" -> {
                    selectedColor = p1.getStringExtra("selectedColor")!!
                    binding.vColorView.setBackgroundColor(Color.parseColor(selectedColor))

                }

                "Yellow" -> {
                    selectedColor = p1.getStringExtra("selectedColor")!!
                    binding.vColorView.setBackgroundColor(Color.parseColor(selectedColor))

                }

                "Purple" -> {
                    selectedColor = p1.getStringExtra("selectedColor")!!
                    binding.vColorView.setBackgroundColor(Color.parseColor(selectedColor))

                }

                "Green" -> {
                    selectedColor = p1.getStringExtra("selectedColor")!!
                    binding.vColorView.setBackgroundColor(Color.parseColor(selectedColor))

                }

                "Orange" -> {
                    selectedColor = p1.getStringExtra("selectedColor")!!
                    binding.vColorView.setBackgroundColor(Color.parseColor(selectedColor))

                }

                "Black" -> {
                    selectedColor = p1.getStringExtra("selectedColor")!!
                    binding.vColorView.setBackgroundColor(Color.parseColor(selectedColor))

                }

                "Image" -> {
                    readStorageTask()
                    binding.lnLayoutWebUrl.visibility = View.GONE
                }

                "WebUrl" -> {
                    // show web url layout
                    binding.lnLayoutWebUrl.visibility = View.VISIBLE
                }

                "DeleteNote" -> {
                    //delete note
                    deleteNote()
                }

                else -> {
                    selectedColor = p1.getStringExtra("selectedColor")!!

                    binding.apply {
                        rlLayoutImage.visibility = View.GONE
                        ivNote.visibility = View.GONE
                        lnLayoutWebUrl.visibility = View.GONE
                        vColorView.setBackgroundColor(Color.parseColor(selectedColor))
                    }
                }
            }
        }
    }

    private fun deleteNote() {
        launch {
            context?.let {
                NotesDatabase.getDatabase(it).noteDao().deleteSpecificNote(noteId)
                requireActivity().supportFragmentManager.popBackStack()
            }
        }
    }

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<out String>,
        grantResults: IntArray
    ) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)

        EasyPermissions.onRequestPermissionsResult(
            requestCode,
            permissions,
            grantResults,
            requireActivity()
        )
    }

    override fun onPermissionsGranted(requestCode: Int, perms: MutableList<String>) {

    }

    override fun onPermissionsDenied(requestCode: Int, perms: MutableList<String>) {
        if (EasyPermissions.somePermissionPermanentlyDenied(requireActivity(), perms)) {
            AppSettingsDialog.Builder(requireActivity()).build().show()
        }
    }

    override fun onRationaleAccepted(requestCode: Int) {

    }

    override fun onRationaleDenied(requestCode: Int) {

    }

    private fun hasReadStoragePerm(): Boolean {
        return EasyPermissions.hasPermissions(
            requireContext(),
            Manifest.permission.READ_EXTERNAL_STORAGE
        )
    }

    private fun readStorageTask() {
        if (hasReadStoragePerm()) {

            pickImageFromGallery()

        } else {
            EasyPermissions.requestPermissions(
                requireActivity(),
                getString(R.string.txt_storage_permission_text),
                READ_STORAGE_PERM,
                Manifest.permission.READ_EXTERNAL_STORAGE
            )
        }
    }

    @SuppressLint("QueryPermissionsNeeded")
    private fun pickImageFromGallery() {
        val intent = Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI)

        if (intent.resolveActivity(requireActivity().packageManager) != null) {
            startActivityForResult(intent, REQUEST_CODE_IMAGE)
        }
    }

    @Deprecated("Deprecated in Java")
    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        if (requestCode == REQUEST_CODE_IMAGE && resultCode == Activity.RESULT_OK) {
            if (data != null) {
                val selectedImageUrl = data.data

                if (selectedImageUrl != null) {
                    try {
                        val inputStream =
                            requireActivity().contentResolver.openInputStream(selectedImageUrl)
                        val bitmap = BitmapFactory.decodeStream(inputStream)

                        binding.apply {
                            ivNote.setImageBitmap(bitmap)
                            ivNote.visibility = View.VISIBLE

                            // TODO: Hiện nơi chứa ảnh ra
                            rlLayoutImage.visibility = View.VISIBLE
                        }

                        selectedImagePath = getPathFromUri(selectedImageUrl)!!
                    } catch (e: Exception) {
                        notify(e.message!!)
                    }

                }
            }
        }
    }

    private fun getPathFromUri(contentUri: Uri): String? {
        var filePath: String? = null
        val cursor = requireActivity().contentResolver.query(contentUri, null, null, null, null)

        if (cursor == null) {
            filePath = contentUri.path
        } else {
            cursor.moveToFirst()

            val index = cursor.getColumnIndex("_data")
            filePath = cursor.getString(index)
            cursor.close()
        }

        return filePath
    }

    private fun notify(s: String) {
        Toast.makeText(context, s, Toast.LENGTH_SHORT).show()
    }

    override fun onDestroyView() {
        // TODO: Unregister broadcast receiver
        LocalBroadcastManager.getInstance(requireContext()).unregisterReceiver(broadcastReceiver)

        super.onDestroyView()
        _binding = null
    }
}