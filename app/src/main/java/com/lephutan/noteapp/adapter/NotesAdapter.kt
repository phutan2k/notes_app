package com.lephutan.noteapp.adapter

import android.annotation.SuppressLint
import android.graphics.BitmapFactory
import android.graphics.Color
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.lephutan.noteapp.R
import com.lephutan.noteapp.databinding.ItemRvNotesBinding
import com.lephutan.noteapp.entities.Notes

class NotesAdapter :
    RecyclerView.Adapter<NotesAdapter.NotesViewHolder>() {

    private var listOfNoteResult: MutableList<Notes> = mutableListOf()
    private var listener: OnItemClickListener? = null

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): NotesViewHolder {
        val binding = ItemRvNotesBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        return NotesViewHolder(binding)
    }

    override fun onBindViewHolder(holder: NotesViewHolder, position: Int) {
        holder.bind(listOfNoteResult[position], position)
    }

    override fun getItemCount(): Int {
        return listOfNoteResult.size
    }

    fun setData(arrNotesList: List<Notes>) {
        listOfNoteResult = arrNotesList as MutableList<Notes>
    }

    fun setOnClickListener(listener: OnItemClickListener){
        this.listener = listener
    }

    inner class NotesViewHolder(private val binding: ItemRvNotesBinding) :
        RecyclerView.ViewHolder(binding.root) {

        @SuppressLint("ResourceAsColor")
        fun bind(noteResult: Notes, position: Int) {
            binding.apply {
                tvTitle.text = noteResult.title
                tvDesc.text = noteResult.noteText
                tvDateTime.text = noteResult.dateTime
            }

            if (noteResult.color != null) {
                binding.cvCardView.setCardBackgroundColor(
                    Color.parseColor(noteResult.color)
                )
            } else {
                binding.cvCardView.setCardBackgroundColor(R.color.colorLightBlack)
            }

            if (noteResult.imgPath != null) {
                val bitmap = BitmapFactory.decodeFile(noteResult.imgPath)

                binding.ivNote.setImageBitmap(bitmap)
                binding.ivNote.visibility = View.VISIBLE
            } else {
                binding.ivNote.visibility = View.GONE
            }

            if (noteResult.webLink != "") {
                binding.tvWebLink.text = noteResult.webLink
                binding.tvWebLink.visibility = View.VISIBLE
            } else {
                binding.tvWebLink.visibility = View.GONE
            }

            binding.cvCardView.setOnClickListener {
                listener!!.onClicked(noteResult.id!!)
            }
        }
    }

    interface OnItemClickListener {
        fun onClicked(noteId: Int)
    }
}