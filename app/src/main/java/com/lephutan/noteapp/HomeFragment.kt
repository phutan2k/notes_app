package com.lephutan.noteapp

import android.annotation.SuppressLint
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.SearchView
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.StaggeredGridLayoutManager
import com.lephutan.noteapp.adapter.NotesAdapter
import com.lephutan.noteapp.database.NotesDatabase
import com.lephutan.noteapp.databinding.FragmentHomeBinding
import com.lephutan.noteapp.entities.Notes
import kotlinx.coroutines.launch
import java.util.*

class HomeFragment : BaseFragment() {
    private var _binding: FragmentHomeBinding? = null
    private val binding get() = _binding!!

    private var adapter: NotesAdapter = NotesAdapter()
    private var listResult: MutableList<Notes> = mutableListOf()

    companion object {
        fun newInstance(): HomeFragment {
            return HomeFragment()
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        _binding = FragmentHomeBinding.inflate(inflater, container, false)

        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        initRecyclerView()
        addNote()
        clickItemView()
        searchNote()
    }

    private fun initRecyclerView() {
        binding.rvRecyclerView.setHasFixedSize(true)
        binding.rvRecyclerView.layoutManager =
            StaggeredGridLayoutManager(2, StaggeredGridLayoutManager.VERTICAL)

        launch {
            context?.let {
                val notes = NotesDatabase.getDatabase(it).noteDao().getAllNotes()

                adapter.setData(notes)
                listResult = notes as MutableList<Notes>
                binding.rvRecyclerView.adapter = adapter
            }
        }
    }

    private fun addNote() {
        binding.fabBtnCreateNote.setOnClickListener {
            replaceFragment(CreateNoteFragment.newInstance(), false)
        }
    }

    private fun searchNote() {
        binding.svSearchView.setOnQueryTextListener(object : SearchView.OnQueryTextListener {
            override fun onQueryTextSubmit(p0: String?): Boolean {
                return true
            }

            @SuppressLint("NotifyDataSetChanged")
            override fun onQueryTextChange(p0: String?): Boolean {
                val tempArr: MutableList<Notes> = mutableListOf()

                for (arr in listResult) {
                    if (arr.title!!.lowercase(Locale.getDefault()).contains(p0.toString())) {
                        tempArr.add(arr)
                    }
                }
                adapter.setData(tempArr)
                adapter.notifyDataSetChanged()

                return true
            }

        })
    }

    private fun clickItemView() {
        adapter.setOnClickListener(onClicked)
    }

    private val onClicked = object : NotesAdapter.OnItemClickListener{
        override fun onClicked(noteId: Int) {
            val fragment: Fragment
            val bundle = Bundle()

            bundle.putInt("noteId", noteId)
            fragment = CreateNoteFragment.newInstance()
            fragment.arguments = bundle

            replaceFragment(fragment, false)
        }
    }

    fun replaceFragment(fragment: Fragment, isTransaction: Boolean) {
        val fragmentTransaction = requireActivity().supportFragmentManager.beginTransaction()

        if (isTransaction) {
            fragmentTransaction.setCustomAnimations(
                android.R.anim.slide_out_right,
                android.R.anim.slide_in_left
            )
        }
        fragmentTransaction.replace(R.id.fl_frame_layout, fragment)
            .addToBackStack(fragment.javaClass.simpleName).commit()
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }
}